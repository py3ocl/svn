#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import io
import subprocess

class Command:

    def __init__(self,
                 result_as_string = True, # If True, use result string instead of stdout/stderr.
                 stdin = None,  # Only valid when result_as_string is False.
                 stdout = None, # Only valid when result_as_string is False.
                 stderr = None, # Only valid when result_as_string is False.
                 shell = False,
                 cwd = None,
                 encoding = None,
                 errors = None,
                 text = None,
                 env = None,
                 close_fds = True,
                 timeout = None):

        self.result_as_string = result_as_string
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.shell = shell
        self.cwd = cwd
        self.encoding = encoding
        self.errors = errors
        self.text = text
        self.env = env
        self.close_fds = close_fds
        self.timeout = timeout
        self.last_error = None

    ## Execute command and return string or output to stdout/stderr.
    def Execute(self, args):
        result = None

        try:
            if self.result_as_string:
                result = subprocess.check_output(args,
                                                 stdin = self.stdin,
                                                 stderr = self.stderr,
                                                 shell = self.shell,
                                                 cwd = self.cwd,
                                                 encoding = self.encoding,
                                                 errors = self.errors,
                                                 text = self.text,
                                                 env = self.env,
                                                 close_fds = self.close_fds,
                                                 timeout = self.timeout)
            else:
                subprocess.run(args,
                               stdin = self.stdin,
                               stdout = self.stdout,
                               stderr = self.stderr,
                               shell = self.shell,
                               cwd = self.cwd,
                               encoding = self.encoding,
                               errors = self.errors,
                               text = self.text,
                               env = self.env,
                               close_fds = self.close_fds,
                               timeout = self.timeout)
        except subprocess.CalledProcessError as e:
            self.last_error = e

        return result
