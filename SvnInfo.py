#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

## Wrapper class for svn info output.

import os
import sys
import subprocess
from Command import Command

class SvnInfo:

    PATH                    = "Path:"
    WORDKING_COPY_ROOT_PATH = "Working Copy Root Path:"
    URL                     = "URL:"
    RELATIVE_URL            = "Relative URL:"
    REPOSITORY_ROOT         = "Repository Root:"
    REPOSITORY_UUID         = "Repository UUID:"
    REVISION                = "Revision:"
    NODE_KIND               = "Node Kind:"
    SCHEDULE                = "Schedule:"
    LAST_CHANGED_AUTHOR     = "Last Changed Author:"
    LAST_CHANGED_REV        = "Last Changed Rev:"
    LAST_CHANGED_DATE       = "Last Changed Date:"

    ## Construct empty list and dictionary.
    def __init__(self):
        self._lines = []
        self._dict  = {}
        self.last_error = None

    ## Get a list containing all the lines returned from svn info.
    def GetLines(self):
        return self._lines

    ## Get the value for the key from the svn info dictionary.
    def GetValue(self, key):
        try:
            value = self._dict[key]
        except:
            value = ""
        return value

    ## Convert string returned by svn info into list and dictionary for fast lookup.
    def SetSvnInfoListFromString(self, svn_info_string, linesep = None):
        if linesep is None:
            linesep = os.linesep
        self._lines = svn_info_string.split(linesep)
        self._dict  = SvnInfo.LinesToDictionary(self._lines)

    ## Execute svn info within current working directory and
    ## set list containing lines and dictionary for fast lookup.
    ## If args is None then "svn info" will be used.
    def SetSvnInfo(self,
                   args = None,    # E.g. "svn info"
                   command = None, # E.g. Command(shell = False, cwd = ".", text = False)
                   shell = False,
                   cwd = None):
        if args is None:
            args = "svn info"

        if cwd is None:
            cwd = "."
        command = command if command else Command(shell = shell, cwd = cwd, text = False)
        svn_info_string = command.Execute(args)
        self.last_error = command.last_error

        if svn_info_string:
            self.SetSvnInfoListFromString(svn_info_string.decode())

        return cwd is not None

    ## Convert lines containing svn info to a dictionary for faster lookup.
    @staticmethod
    def LinesToDictionary(lines):
        dict = {}
        for line in lines:
            pos = line.find(":")
            if pos != -1:
                key = line[:pos + 1]
                value = line[pos + 1:].lstrip()
                dict[key] = value
        return dict

if __name__ == '__main__':

    if len(sys.argv) > 1:
        svn_info = SvnInfo()
        path_found = svn_info.SetSvnInfo(cwd = sys.argv[1])

        if path_found:
            key = ""

            # If additional arguments beyond the path are provided, use these as the key
            # for printing the value.
            if len(sys.argv) > 2:
                args = sys.argv[2:]
                for arg in args:
                    key += arg + " "
                key = key.rstrip()

            if key:
                value = svn_info.GetValue(key)
                if value:
                    print(key)
                    print(value)
                else:
                    print("'{}' not found.".format(key), file=sys.stderr)
            else:
                for line in svn_info.GetLines():
                    print(line)
        else:
            print("path not found.", file=sys.stderr)
    else:
        print("path or . required", file=sys.stderr)
